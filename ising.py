import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import convolve, generate_binary_structure

N = 50
prob1 = 0.75
prob2 = 0.25

init_random = np.random.random((N, N))
lattice_n = np.zeros((N, N))
lattice_n[init_random >= prob1] = 1
lattice_n[init_random < prob1] = -1

init_random = np.random.random((N, N))
lattice_p = np.zeros((N, N))
lattice_p[init_random >= prob2] = 1
lattice_p[init_random < prob2] = -1

def get_energy(lattice):
    kern = generate_binary_structure(2, 1)
    kern[1][1] = False
    arr = -lattice * convolve(lattice, kern, mode = 'constant')
    return arr.sum()

def metropolis(spin_arr, times, BJ, energy, draw_board = False):
    spin_arr = spin_arr.copy()
    net_spins = np.zeros(times - 1)
    net_energy = np.zeros(times - 1)

    for t in range(0, times - 1):
        x = np.random.randint(0, N)
        y = np.random.randint(0, N)
        spin_i = spin_arr[x, y]
        spin_f = -1 * spin_i
        E_i = 0
        E_f = 0

        if x > 0:
            E_i += -spin_i * spin_arr[x - 1, y]
            E_f += -spin_f * spin_arr[x - 1, y]
        if x < N - 1:
            E_i += -spin_i * spin_arr[x + 1, y]
            E_f += -spin_f * spin_arr[x + 1, y]
        if y > 0:
            E_i += -spin_i * spin_arr[x, y - 1]
            E_f += -spin_f * spin_arr[x, y - 1]
        if y < N - 1:
            E_i += -spin_i * spin_arr[x, y + 1]
            E_f += -spin_f * spin_arr[x, y + 1]
        
        dE = E_f - E_i
        if (dE > 0) * (np.random.random() < np.exp(-BJ * dE)):
            spin_arr[x, y]=spin_f
            energy += dE
        elif dE<=0:
            spin_arr[x, y]=spin_f
            energy += dE

        net_spins[t] = spin_arr.sum()
        net_energy[t] = energy

        if draw_board == True:
            plt.ion()
            plt.imshow(np.where(spin_arr.reshape(N, N) < 1, 1, 0), cmap = 'gray')
            plt.pause(0.1)
            plt.draw()
            plt.clf()
    return net_spins

betas = [0.1, 0.2, 0.3, 0.5, 0.6]
spins = []

for b in betas:
    spin_i = metropolis(lattice_n, 1000000, b, get_energy(lattice_n))
    spins.append(spin_i)

fig, ax = plt.subplots(1, 1, figsize = (12, 4))
for i in range (len(spins)):
    ax.plot(spins[i] / N ** 2)
ax.set_xlabel('Algorithm Time Steps')
ax.set_ylabel(r'Average Spin $\bar{m}$')
ax.grid()
plt.show()

def get_spin_energy(lattice, BJs):
    ms = np.zeros(len(BJs))
    for i, bj in enumerate(BJs):
        spins = metropolis(lattice, 1000000, bj, get_energy(lattice))
        ms[i] = spins[-100000 : ].mean() / N  ** 2
    return ms
    
BJs = np.arange(0.1, 2, 0.05)
ms_n = get_spin_energy(lattice_n, BJs)
ms_p = get_spin_energy(lattice_p, BJs)

plt.figure(figsize = (8, 5))
plt.plot(1 / BJs, ms_n, 'o--', label = '75% of spins started negative')
plt.plot(1 / BJs, ms_p, 'o--', label = '75% of spins started positive')
plt.xlabel(r'$\left(\frac{k}{J}\right)T$')
plt.ylabel(r'$\bar{m}$')
plt.legend(facecolor = 'white', framealpha = 1)
plt.show()