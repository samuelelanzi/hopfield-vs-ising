import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

n = 50
N = n * n
temp = 0

def image_to_np(path):
    im = Image.open(path)
    im_np = np.asarray(im)
    try:
        im_np = im_np[:, :, 0]
    except IndexError:
        pass
    im_np = np.where(im_np < n, 1, -1)
    im_np = im_np.reshape(N)
    return im_np

def noisy_pattern(pattern, frac = 0.4):
    return np.array([item if np.random.random() > frac else -item for item in pattern])

def half_pattern(pattern, frac = 0.7):
    return np.array([pattern[item] if item < frac * N else -1 for item in range(N)])

def create_weights_matrix(patterns):
    return sum([np.outer(p, p) - np.identity(N) for p in patterns])

def delta_H(i, J, spins):
    return 2 * np.matmul(J, spins)[i] * spins[i]

def glauber(T, spins, weight_m):
    def single_evolve(j):
        dE = delta_H(j, weight_m, spins)
        if np.random.random() < ( 1 + np.exp(dE / T) ) ** (-1):
            spins[j] *= -1
    index = np.random.choice(a = range(N))
    single_evolve(index)

def update_network(t, T, spins, weight_m, draw_board = False):
    for i in range(t):
        glauber(T, spins, weight_m)
        if draw_board == True:
            plt.ion()
            plt.imshow(np.where(spins.reshape(n, n) < 1, 1, 0), cmap = 'gray')
            plt.pause(0.001)
            plt.draw()
            plt.clf()

pattern_1 = image_to_np('patterns/pattern_gioconda.jpeg')
pattern_2 = image_to_np('patterns/pattern_newton.jpeg')
pattern_3 = image_to_np('patterns/pattern_viandante.jpeg')

patterns = np.asarray([pattern_1])
patterns = np.append(patterns, [pattern_2], axis = 0)
patterns = np.append(patterns, [pattern_3], axis = 0)

pattern_noise = noisy_pattern(patterns[np.random.choice(len(patterns))])
half_pattern = half_pattern(patterns[np.random.choice(len(patterns))])

W = create_weights_matrix(patterns)

patterns = np.append(patterns, [pattern_noise], axis = 0)
update_network(100000, temp, pattern_noise, W)
patterns = np.append(patterns, [pattern_noise], axis = 0)

# for i in range(len(patterns)):
#     plt.subplot(2, 3, i + 1)
#     plt.imshow(np.where(patterns[i].reshape(n, n) < 1, 1, 0), cmap = 'gray')
# plt.show() 